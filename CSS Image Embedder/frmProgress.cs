﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace CSS_Image_Embedder {
    public partial class frmProgress : Form {
        // Initialize Cancel event delegate with empty function
        public EventHandler Cancel = (sender, e) => { };

        public frmProgress() {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new frmProgress with some additional options.
        /// </summary>
        /// <param name="status">The status to start with.</param>
        /// <param name="maxProgress">The progress bar's maximum. (Optional)</param>
        /// <param name="supportsCancel">Whether or not Cancel is supported. Be sure to bind the Cancel event handler if supportsCancel is set to true.</param>
        public frmProgress(String status, int maxProgress = 100, Boolean supportsCancel = false) : this() {
            SetStatus(status);
            SetMax(maxProgress);

            if (supportsCancel)
                btnCancel.Enabled = true;
        }

        private void frmProgress_Shown(object sender, EventArgs e) {
            // Refresh the GUI
            this.Refresh();

            this.TopMost = false;
            this.BringToFront();
        }

        /// <summary>
        /// Closes the window after the given number of seconds.
        /// </summary>
        /// <param name="seconds">The number of seconds to wait until the window is closed.</param>
        public void CloseAfter(int seconds, Form parent) {
            Thread.Sleep(seconds);
            parent.TopMost = true;
            ForceClose();
            parent.TopMost = false;
        }

        /// <summary>
        /// Forcefully closes the window.
        /// </summary>
        public void ForceClose() {
            this.FormClosing -= frmProgress_FormClosing;
            this.Close();
        }

        /// <summary>
        /// Sets the progressbar to indeterminate (Marquee) mode.
        /// </summary>
        public void SetIndeterminate() {
            // Start progress bar marquee
            pbMain.Style = ProgressBarStyle.Marquee;
        }

        /// <summary>
        /// Sets the progressbar to determiante (Blocks) mode.
        /// </summary>
        public void SetDeterminate() {
            pbMain.Style = ProgressBarStyle.Blocks;
        }

        /// <summary>
        /// Sets the progress dialog's status.
        /// </summary>
        /// <param name="strStatus">The status.</param>
        public void SetStatus(String strStatus) {
            lblStatus.Text = strStatus;
        }

        /// <summary>
        /// Sets the progress dialog's title.
        /// </summary>
        /// <param name="strTitle">The title.</param>
        public void SetTitle(String strTitle) {
            this.Text = strTitle;
        }

        /// <summary>
        /// Sets the progress dialog to error mode.
        /// </summary>
        public void SetError() {
            this.Text = "Error";
            pbMain.Style = ProgressBarStyle.Blocks;
            epMain.SetError(lblStatus, lblStatus.Text);
        }

        /// <summary>
        /// Sets the progressbar's progress.
        /// </summary>
        /// <param name="progress">Progress</param>
        public void SetProgress(int progress) {
            pbMain.Value = progress;
        }

        /// <summary>
        /// Sets the progressbar's maximum value.
        /// </summary>
        /// <param name="max">The max value.</param>
        public void SetMax(int max) {
            SetDeterminate();
            pbMain.Maximum = max;
        }

        /// <summary>
        /// Sets whether the progress window allows Cancel.
        /// </summary>
        /// <param name="allowsCancel">Whether or not to allow Cancel.</param>
        public void SetAllowsCancel(Boolean allowsCancel) {
            btnCancel.Enabled = allowsCancel;
        }

        // Prevent a user from closing the progress dialog
        private void frmProgress_FormClosing(object sender, FormClosingEventArgs e) {
            e.Cancel = true;
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            // Invoke the custom Cancel event
            Cancel(sender, e);

            SetStatus("Aborting operation...");
            SetIndeterminate();

            btnCancel.Enabled = false;
        }

    }
}