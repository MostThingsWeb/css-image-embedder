﻿namespace CSS_Image_Embedder {
    partial class frmMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.miFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.miClose = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miEmbedAndSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miAutoResolve = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.miHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.miAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.divider1 = new System.Windows.Forms.GroupBox();
            this.lblFilesReferenced = new System.Windows.Forms.Label();
            this.lvFiles = new System.Windows.Forms.ListView();
            this.chName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblCSS = new System.Windows.Forms.Label();
            this.rtbCSS = new System.Windows.Forms.RichTextBox();
            this.ofdCSS = new System.Windows.Forms.OpenFileDialog();
            this.btnAutoResolve = new System.Windows.Forms.Button();
            this.pbPreview = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnUnresolve = new System.Windows.Forms.Button();
            this.ttMain = new System.Windows.Forms.ToolTip(this.components);
            this.btnManuallyResovle = new System.Windows.Forms.Button();
            this.ofdOther = new System.Windows.Forms.OpenFileDialog();
            this.lblStats = new System.Windows.Forms.Label();
            this.sfdCSS = new System.Windows.Forms.SaveFileDialog();
            this.msMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(306, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inlines image references in CSS files via the data:// url scheme.";
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miHelp});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(756, 24);
            this.msMain.TabIndex = 1;
            this.msMain.Text = "menuStrip1";
            // 
            // miFile
            // 
            this.miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOpen,
            this.miClose,
            this.toolStripSeparator1,
            this.miEmbedAndSave,
            this.toolStripSeparator2,
            this.miAutoResolve,
            this.toolStripSeparator3,
            this.miExit});
            this.miFile.Name = "miFile";
            this.miFile.Size = new System.Drawing.Size(37, 20);
            this.miFile.Text = "&File";
            // 
            // miOpen
            // 
            this.miOpen.Name = "miOpen";
            this.miOpen.Size = new System.Drawing.Size(149, 22);
            this.miOpen.Text = "Open...";
            this.miOpen.ToolTipText = "Opens a CSS file.";
            this.miOpen.Click += new System.EventHandler(this.miOpen_Click);
            // 
            // miClose
            // 
            this.miClose.Name = "miClose";
            this.miClose.Size = new System.Drawing.Size(149, 22);
            this.miClose.Text = "&Close";
            this.miClose.ToolTipText = "Close the current CSS file.";
            this.miClose.Click += new System.EventHandler(this.miClose_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(146, 6);
            // 
            // miEmbedAndSave
            // 
            this.miEmbedAndSave.Name = "miEmbedAndSave";
            this.miEmbedAndSave.Size = new System.Drawing.Size(149, 22);
            this.miEmbedAndSave.Text = "Save...";
            this.miEmbedAndSave.ToolTipText = "Embeds the resolved images and saves the CSS.";
            this.miEmbedAndSave.Click += new System.EventHandler(this.miSave_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(146, 6);
            // 
            // miAutoResolve
            // 
            this.miAutoResolve.Name = "miAutoResolve";
            this.miAutoResolve.Size = new System.Drawing.Size(149, 22);
            this.miAutoResolve.Text = "Auto resolve...";
            this.miAutoResolve.ToolTipText = "Automatically resolves multiple images by the same name.";
            this.miAutoResolve.Click += new System.EventHandler(this.miAutoResolve_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(146, 6);
            // 
            // miExit
            // 
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(149, 22);
            this.miExit.Text = "Exit";
            this.miExit.ToolTipText = "Exit CSS Image Embedder.";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // miHelp
            // 
            this.miHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAbout});
            this.miHelp.Name = "miHelp";
            this.miHelp.Size = new System.Drawing.Size(44, 20);
            this.miHelp.Text = "&Help";
            // 
            // miAbout
            // 
            this.miAbout.Name = "miAbout";
            this.miAbout.Size = new System.Drawing.Size(152, 22);
            this.miAbout.Text = "&About";
            this.miAbout.Click += new System.EventHandler(this.miAbout_Click);
            // 
            // divider1
            // 
            this.divider1.Location = new System.Drawing.Point(3, 46);
            this.divider1.Name = "divider1";
            this.divider1.Size = new System.Drawing.Size(856, 5);
            this.divider1.TabIndex = 2;
            this.divider1.TabStop = false;
            // 
            // lblFilesReferenced
            // 
            this.lblFilesReferenced.AutoSize = true;
            this.lblFilesReferenced.Location = new System.Drawing.Point(9, 70);
            this.lblFilesReferenced.Name = "lblFilesReferenced";
            this.lblFilesReferenced.Size = new System.Drawing.Size(98, 13);
            this.lblFilesReferenced.TabIndex = 3;
            this.lblFilesReferenced.Text = "Images referenced:";
            // 
            // lvFiles
            // 
            this.lvFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chName,
            this.chStatus});
            this.lvFiles.Enabled = false;
            this.lvFiles.FullRowSelect = true;
            this.lvFiles.Location = new System.Drawing.Point(12, 86);
            this.lvFiles.MultiSelect = false;
            this.lvFiles.Name = "lvFiles";
            this.lvFiles.Size = new System.Drawing.Size(369, 149);
            this.lvFiles.TabIndex = 4;
            this.lvFiles.UseCompatibleStateImageBehavior = false;
            this.lvFiles.View = System.Windows.Forms.View.Details;
            this.lvFiles.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lvFiles_ItemSelectionChanged);
            this.lvFiles.Enter += new System.EventHandler(this.lvFiles_Enter);
            this.lvFiles.Leave += new System.EventHandler(this.lvFiles_Leave);
            // 
            // chName
            // 
            this.chName.Text = "Name";
            this.chName.Width = 111;
            // 
            // chStatus
            // 
            this.chStatus.Text = "Status";
            this.chStatus.Width = 91;
            // 
            // lblCSS
            // 
            this.lblCSS.AutoSize = true;
            this.lblCSS.Location = new System.Drawing.Point(12, 269);
            this.lblCSS.Name = "lblCSS";
            this.lblCSS.Size = new System.Drawing.Size(31, 13);
            this.lblCSS.TabIndex = 5;
            this.lblCSS.Text = "CSS:";
            // 
            // rtbCSS
            // 
            this.rtbCSS.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.rtbCSS.DetectUrls = false;
            this.rtbCSS.Location = new System.Drawing.Point(12, 285);
            this.rtbCSS.Name = "rtbCSS";
            this.rtbCSS.ReadOnly = true;
            this.rtbCSS.Size = new System.Drawing.Size(731, 171);
            this.rtbCSS.TabIndex = 6;
            this.rtbCSS.Text = "";
            // 
            // ofdCSS
            // 
            this.ofdCSS.Filter = "CSS files|*.css|Text files|*.txt";
            // 
            // btnAutoResolve
            // 
            this.btnAutoResolve.Enabled = false;
            this.btnAutoResolve.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAutoResolve.Location = new System.Drawing.Point(387, 161);
            this.btnAutoResolve.Name = "btnAutoResolve";
            this.btnAutoResolve.Size = new System.Drawing.Size(120, 23);
            this.btnAutoResolve.TabIndex = 7;
            this.btnAutoResolve.Text = "Auto resolve...";
            this.ttMain.SetToolTip(this.btnAutoResolve, "Automatically resolves multiple images by the same name.");
            this.btnAutoResolve.UseVisualStyleBackColor = true;
            this.btnAutoResolve.Click += new System.EventHandler(this.btnAutoResolve_Click);
            // 
            // pbPreview
            // 
            this.pbPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbPreview.Location = new System.Drawing.Point(3, 16);
            this.pbPreview.Name = "pbPreview";
            this.pbPreview.Size = new System.Drawing.Size(197, 146);
            this.pbPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbPreview.TabIndex = 8;
            this.pbPreview.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pbPreview);
            this.groupBox1.Location = new System.Drawing.Point(543, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 165);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Image Preview";
            // 
            // btnUnresolve
            // 
            this.btnUnresolve.Enabled = false;
            this.btnUnresolve.Location = new System.Drawing.Point(387, 86);
            this.btnUnresolve.Name = "btnUnresolve";
            this.btnUnresolve.Size = new System.Drawing.Size(120, 23);
            this.btnUnresolve.TabIndex = 11;
            this.btnUnresolve.Text = "Unresolve";
            this.ttMain.SetToolTip(this.btnUnresolve, "Removes the file resolution for this reference.");
            this.btnUnresolve.UseVisualStyleBackColor = true;
            this.btnUnresolve.Click += new System.EventHandler(this.btnUnresolve_Click);
            // 
            // btnManuallyResovle
            // 
            this.btnManuallyResovle.Enabled = false;
            this.btnManuallyResovle.Location = new System.Drawing.Point(387, 115);
            this.btnManuallyResovle.Name = "btnManuallyResovle";
            this.btnManuallyResovle.Size = new System.Drawing.Size(120, 22);
            this.btnManuallyResovle.TabIndex = 12;
            this.btnManuallyResovle.Text = "Manually resolve...";
            this.ttMain.SetToolTip(this.btnManuallyResovle, "Manually resolve a single image to the selected reference.");
            this.btnManuallyResovle.UseVisualStyleBackColor = true;
            this.btnManuallyResovle.Click += new System.EventHandler(this.btnManuallyResolve_Click);
            // 
            // lblStats
            // 
            this.lblStats.AutoSize = true;
            this.lblStats.Location = new System.Drawing.Point(12, 238);
            this.lblStats.Name = "lblStats";
            this.lblStats.Size = new System.Drawing.Size(108, 13);
            this.lblStats.TabIndex = 13;
            this.lblStats.Text = "Total: 0 | Resolved: 0";
            // 
            // sfdCSS
            // 
            this.sfdCSS.Filter = "CSS files|*.css|Text files|*.txt";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 468);
            this.Controls.Add(this.lblStats);
            this.Controls.Add(this.btnManuallyResovle);
            this.Controls.Add(this.btnUnresolve);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnAutoResolve);
            this.Controls.Add(this.rtbCSS);
            this.Controls.Add(this.lblCSS);
            this.Controls.Add(this.lvFiles);
            this.Controls.Add(this.lblFilesReferenced);
            this.Controls.Add(this.divider1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.msMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.msMain;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CSS Image Embedder";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.ToolStripMenuItem miHelp;
        private System.Windows.Forms.ToolStripMenuItem miAbout;
        private System.Windows.Forms.GroupBox divider1;
        private System.Windows.Forms.Label lblFilesReferenced;
        private System.Windows.Forms.ListView lvFiles;
        private System.Windows.Forms.ColumnHeader chName;
        private System.Windows.Forms.ColumnHeader chStatus;
        private System.Windows.Forms.Label lblCSS;
        private System.Windows.Forms.RichTextBox rtbCSS;
        private System.Windows.Forms.ToolStripMenuItem miOpen;
        private System.Windows.Forms.OpenFileDialog ofdCSS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem miEmbedAndSave;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem miAutoResolve;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.Button btnAutoResolve;
        private System.Windows.Forms.PictureBox pbPreview;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnUnresolve;
        private System.Windows.Forms.ToolTip ttMain;
        private System.Windows.Forms.Button btnManuallyResovle;
        private System.Windows.Forms.OpenFileDialog ofdOther;
        private System.Windows.Forms.Label lblStats;
        private System.Windows.Forms.ToolStripMenuItem miClose;
        private System.Windows.Forms.SaveFileDialog sfdCSS;
    }
}

