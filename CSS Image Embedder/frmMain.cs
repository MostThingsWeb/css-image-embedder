﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CSS_Image_Embedder {
    public partial class frmMain : Form {
        public frmMain() {
            InitializeComponent();
        }

        private String originalTitle = "";
        private String activeFileName = "";
        private Boolean fileIsOpen = false;

        private frmProgress progressDialog;

        // The last highlighted file
        private int lastSelStart = -1;
        private int lastSelLen = -1;

        #region Form Load

        private void frmMain_Load(object sender, EventArgs e) {
            // Remember the initial window title
            originalTitle = this.Text;

            UpdateUI();
        }

        #endregion

        #region Dirty State

        private Boolean dirtyState = false;

        private void SetDirty() {
            dirtyState = true;
            this.Text = originalTitle + " - " + activeFileName + "*";
        }

        private void ClearDirty() {
            dirtyState = false;
            this.Text = originalTitle + " - " + activeFileName;
        }

        #endregion

        #region File Encoding routine

        public String EncodeFile(String path) {
            // Read the file
            byte[] contents = File.ReadAllBytes(path);

            // Encode as Base64
            String base64Contents = Convert.ToBase64String(contents);

            // Get the mime type
            // Modified from: http://www.pinvoke.net/default.aspx/urlmon/findmimefromdata.html

            IntPtr mimeout;

            int sampleSize = (int)contents.Length;

            if (sampleSize > 4096)
                sampleSize = 4096;

            // Trim the contents array
            byte[] buf = new byte[sampleSize];

            for (int i = 0; i < sampleSize; i++)
                buf[i] = contents[i];

            int result = FindMimeFromData(IntPtr.Zero, path, buf, sampleSize, null, 0, out mimeout, 0);

            if (result != 0) {
                Exception ex = Marshal.GetExceptionForHR(result);

                throw new Exception("Mime-type resolution failed.", ex);
            }

            String mimeType = Marshal.PtrToStringUni(mimeout);

            Marshal.FreeCoTaskMem(mimeout);

            // Piece together everything
            return mimeType + ";base64," + base64Contents;
        }

        #endregion

        #region Opening, Saving, and Processing CSS

        private void OpenCSS() {
            // Open open file dialog
            if (ofdCSS.ShowDialog() == DialogResult.OK) {
                if (dirtyState && MessageBox.Show("Are you sure you want to abandon your progress on the current CSS file?", "CSS Image Embedder", MessageBoxButtons.YesNo) == DialogResult.No)
                    return;

                dirtyState = false;

                String path = ofdCSS.FileName;

                // Get the filename from the path
                activeFileName = (new FileInfo(path)).Name;
                fileIsOpen = true;

                this.Enabled = false;

                // Make a new progress window if none exists or if the current one is hidden
                if (progressDialog == null || !progressDialog.Visible)
                    progressDialog = new frmProgress();

                progressDialog.SetStatus("Reading file...");
                progressDialog.SetMax(100);
                progressDialog.SetAllowsCancel(true);

                // Clear the CSS box
                rtbCSS.Text = "";

                // Create a backgroundworker to handle the read operation
                BackgroundWorker bwLoadFile = new BackgroundWorker();

                bwLoadFile.WorkerReportsProgress = true;
                bwLoadFile.WorkerSupportsCancellation = true;

                EventHandler CancelEventHandler = (sender, e) => bwLoadFile.CancelAsync();

                // Bind "Cancel" event handler
                progressDialog.Cancel += CancelEventHandler;

                // The background worker will report progress
                bwLoadFile.ProgressChanged += (senderBW, eBW) => {
                    switch (eBW.ProgressPercentage) {
                        case (int)BW_State.Progress:
                            progressDialog.SetProgress(eBW.ProgressPercentage);
                            progressDialog.Refresh();
                            break;

                        case (int)BW_State.Done:
                            rtbCSS.Text = (String)eBW.UserState;
                            rtbCSS.Enabled = true;
                            lvFiles.Enabled = true;

                            this.Text = originalTitle + " - " + activeFileName;

                            progressDialog.Cancel -= CancelEventHandler;

                            // Now do syntax coloring
                            ProcessCSS();
                            break;

                        case (int)BW_State.Error:
                            progressDialog.SetError();
                            progressDialog.SetStatus((String)eBW.UserState);
                            progressDialog.Refresh();
                            this.Enabled = true;
                            activeFileName = "";
                            fileIsOpen = false;
                            progressDialog.Cancel -= CancelEventHandler;
                            progressDialog.CloseAfter(3000, this);
                            break;

                        case (int)BW_State.Canceled:
                            progressDialog.SetError();
                            progressDialog.SetStatus("Operation aborted by user.");
                            progressDialog.Refresh();
                            this.Enabled = true;
                            activeFileName = "";
                            fileIsOpen = false;
                            progressDialog.Cancel -= CancelEventHandler;
                            progressDialog.CloseAfter(2000, this);
                            break;
                    }
                };

                bwLoadFile.DoWork += (senderBW, eBW) => {
                    // If a cancellation is pending, report it and return
                    // from this method
                    if (bwLoadFile.CancellationPending) {
                        eBW.Cancel = true;
                        bwLoadFile.ReportProgress((int)BW_State.Canceled);
                        return;
                    }

                    // http://stackoverflow.com/questions/2161895/reading-large-text-files-with-streams-in-c
                    const int bufferSize = 1024;

                    // Will contain entire file contents
                    StringBuilder sb = new StringBuilder();

                    Char[] buffer = new Char[bufferSize];

                    // Attempt to read the data
                    try {
                        long length = 0;
                        long totalRead = 0;
                        int count = bufferSize;

                        // Used for estimating percent complete
                        int reads = 0;
                        int totalReads = 0;

                        StreamReader sr = new StreamReader(path);
                        length = sr.BaseStream.Length;

                        totalReads = (int)(length / bufferSize) + 1;

                        int percentComplete = 0;

                        while (count > 0) {
                            if (bwLoadFile.CancellationPending) {
                                eBW.Cancel = true;
                                bwLoadFile.ReportProgress((int)BW_State.Canceled);
                                return;
                            }

                            count = sr.Read(buffer, 0, bufferSize);
                            sb.Append(buffer, 0, count);
                            totalRead += count;

                            // Calculate percent complete
                            percentComplete = (reads * 100) / totalReads;

                            // If done, also report contents of string builder
                            if (percentComplete == 100)
                                bwLoadFile.ReportProgress((int)BW_State.Done, sb.ToString());
                            else
                                bwLoadFile.ReportProgress((int)BW_State.Progress, percentComplete);

                            reads++;
                        }
                    } catch (Exception ex) {
                        // Report failure
                        bwLoadFile.ReportProgress((int)BW_State.Error, "Internal error: Operation failed.");
                        return;
                    }
                };

                // Begin
                progressDialog.Show();
                bwLoadFile.RunWorkerAsync();
            }
        }

        private void ProcessCSS() {
            progressDialog.SetStatus("Analyzing CSS...");
            progressDialog.SetMax(100);
            progressDialog.SetAllowsCancel(false);

            BackgroundWorker bwFileFinder = new BackgroundWorker();

            bwFileFinder.WorkerReportsProgress = true;
            bwFileFinder.WorkerSupportsCancellation = true;

            // Get the css
            String css = rtbCSS.Text;

            // Clear any existing file references from the list
            lvFiles.Items.Clear();

            // Keeps track of filenames that we were unable to parse
            List<String> failures = new List<String>();
            // Keeps track of all the file references so we don't end up with duplicates
            List<String> alreadyAdded = new List<String>();

            bwFileFinder.ProgressChanged += (senderBW, eBW) => {
                switch (eBW.ProgressPercentage) {
                    case (int)BW_State.Result:
                        // A file url was found
                        CSSReference file = (CSSReference)eBW.UserState;

                        if (!alreadyAdded.Contains(file.Name)) {
                            alreadyAdded.Add(file.Name);

                            // Highlight it in the CSS
                            rtbCSS.Select(file.Start, file.Length);
                            rtbCSS.SelectionColor = Color.Red;

                            ListViewItem item = new ListViewItem(new String[] { file.Name, "Unresolved" });

                            item.ForeColor = Color.Red;
                            item.Tag = file;

                            lvFiles.Items.Add(item);
                        }
                        break;
                    case (int)BW_State.Progress:
                        // The percentage needs to be updated
                        int percentage = (int)eBW.UserState;
                        progressDialog.SetProgress(percentage);

                        if (percentage == 100) {
                            CloseProgressDialog(progressDialog);
                            this.Enabled = true;

                            lvFiles.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);

                            // Report any failures
                            if (failures.Count > 0) {
                                // Generate a message
                                String msg = "One or more image references were found that could not be parsed, including:\n";

                                int sample = Math.Min(failures.Count, 3);

                                for (int i = 0; i < sample; i++)
                                    msg += failures[i] + "\n";

                                MessageBox.Show(msg);
                            }

                            // Clear any selections
                            rtbCSS.SelectionStart = 0;
                            rtbCSS.SelectionLength = 0;
                            rtbCSS.ScrollToCaret();

                            UpdateStats();
                            UpdateUI();
                        }

                        break;
                    case (int)BW_State.Notice:
                        // No image references were found
                        MessageBox.Show((String)eBW.UserState);
                        CloseProgressDialog(progressDialog);
                        this.Enabled = true;
                        break;
                    case (int)BW_State.Failure:
                        // An image reference couldn't be parsed
                        failures.Add(eBW.UserState.ToString());
                        break;
                }
            };

            // Define a Regex for finding the images
            Regex urlRegex = new Regex(@"url\(([^\s]+)\)");

            bwFileFinder.DoWork += (senderBW, eBW) => {
                // Find file urls in the CSS
                MatchCollection files = urlRegex.Matches(css);

                int fileLen = files.Count;

                // If no URLs were found, exit
                if (fileLen == 0) {
                    bwFileFinder.ReportProgress((int)BW_State.Notice, "No image references were found in this CSS file.");
                    return;
                }

                int curFile = 1;

                foreach (Match file in files) {
                    CSSReference ret = new CSSReference();

                    // Define the reference
                    ret.Start = file.Index;
                    ret.Length = file.Length;
                    ret.Name = file.Groups[1].Value.ToLower();
                    ret.Resolved = false;

                    // Clean up the file name
                    ret.Name = ret.Name.Trim().Trim(new char[] { '\'', '"' });
                    ret.Name = ret.Name.TrimEnd(new char[] { '\\', '/' });

                    // Get just the filename part from the path
                    try {
                        FileInfo info = new FileInfo(ret.Name);
                        ret.ShortName = info.Name.Trim().ToLower();

                        // Report the file
                        bwFileFinder.ReportProgress((int)BW_State.Result, ret);
                    } catch (Exception ex) {
                        // Failure
                        bwFileFinder.ReportProgress((int)BW_State.Failure, ret.Name);
                    } finally {
                        // Report our progress
                        bwFileFinder.ReportProgress((int)BW_State.Progress, (curFile * 100) / fileLen);

                        curFile++;
                    }
                }
            };

            progressDialog.Show();
            bwFileFinder.RunWorkerAsync();
        }

        private void SaveCSS() {
            if (sfdCSS.ShowDialog() == DialogResult.OK) {
                String path = sfdCSS.FileName;

                String data = rtbCSS.Text;

                this.Enabled = false;

                // Make a new progress window if none exists or if the current one is hidden
                if (progressDialog == null || !progressDialog.Visible)
                    progressDialog = new frmProgress();

                progressDialog.SetStatus("Embedding images and saving CSS...");
                progressDialog.SetMax(100);
                progressDialog.SetAllowsCancel(true);

                BackgroundWorker bwSave = new BackgroundWorker();

                bwSave.WorkerReportsProgress = true;
                bwSave.WorkerSupportsCancellation = true;

                // Contains a list of references that were unable to be encoded
                List<String> failures = new List<String>();

                EventHandler CancelEventHandler = (sender, e) => bwSave.CancelAsync();

                // Bind "Cancel" event handler
                progressDialog.Cancel += CancelEventHandler;

                bwSave.ProgressChanged += (senderBW, eBW) => {
                    switch (eBW.ProgressPercentage) {
                        case (int)BW_State.Failure:
                            // A file couldn't be encoded
                            failures.Add((String)eBW.UserState);
                            break;

                        case (int)BW_State.Progress:
                            // Update progress
                            progressDialog.SetProgress((int)eBW.UserState);
                            progressDialog.Refresh();

                            if ((int)eBW.UserState == 100) {
                                progressDialog.Cancel -= CancelEventHandler;
                                CloseProgressDialog(progressDialog);
                                dirtyState = false;
                                this.Text = originalTitle + " - " + activeFileName;

                                this.Enabled = true;
                            }
                            break;

                        case (int)BW_State.Error:
                            // The CSS couldn't be saved
                            progressDialog.SetError();
                            progressDialog.SetStatus((String)eBW.UserState);
                            progressDialog.Refresh();
                            this.Enabled = true;
                            progressDialog.Cancel -= CancelEventHandler;
                            progressDialog.CloseAfter(2000, this);
                            break;

                        case (int)BW_State.Canceled:
                            progressDialog.SetError();
                            progressDialog.SetStatus("Operation aborted by user.");
                            progressDialog.Refresh();
                            this.Enabled = true;
                            progressDialog.Cancel -= CancelEventHandler;
                            progressDialog.CloseAfter(2000, this);
                            break;
                    }
                };

                // Keys: the file to search for | Value: the path to encode and replace
                Dictionary<String, String> toEmbed = new Dictionary<String, String>();

                // Collect a list of files that have been resolved
                for (int i = 0; i < lvFiles.Items.Count; i++) {
                    ListViewItem item = lvFiles.Items[i];
                    CSSReference url = (CSSReference)item.Tag;

                    if (url.Resolved)
                        toEmbed.Add(url.Name, url.RealPath);
                }

                bwSave.DoWork += (senderBW, eBW) => {
                    // If a cancellation is pending, report it and return
                    // from this method
                    if (bwSave.CancellationPending) {
                        eBW.Cancel = true;
                        bwSave.ReportProgress((int)BW_State.Canceled);
                        return;
                    }

                    int totalEmbeds = toEmbed.Count;
                    int embedIndex = 1;

                    // Iterate through each item that we need to embed
                    foreach (KeyValuePair<String, String> item in toEmbed) {
                        if (bwSave.CancellationPending) {
                            eBW.Cancel = true;
                            bwSave.ReportProgress((int)BW_State.Canceled);
                            return;
                        }

                        String search = item.Key;
                        String replace = null;

                        try {
                            // Encode the file
                            replace = EncodeFile(item.Value);
                        } catch (Exception ex) {
                            // Reporty and skip this reference
                            bwSave.ReportProgress((int)BW_State.Failure, search);
                            continue;
                        }

                        // Perform replace
                        data = data.Replace(search, "'data:" + replace + "'");

                        // Report percentage
                        int progressPercent = (embedIndex * 100) / totalEmbeds;

                        // Save 100% progress for when the CSS has been saved as well
                        if (progressPercent == 100)
                            progressPercent = 99;

                        bwSave.ReportProgress((int)BW_State.Progress, progressPercent);

                        embedIndex++;
                    }

                    if (bwSave.CancellationPending) {
                        eBW.Cancel = true;
                        bwSave.ReportProgress((int)BW_State.Canceled);
                        return;
                    }

                    try {
                        File.WriteAllText(path, data);
                    } catch (Exception ex) {
                        // Unable to write CSS
                        bwSave.ReportProgress((int)BW_State.Error, "Unable to save CSS.");
                    }

                    // 100% progress
                    bwSave.ReportProgress((int)BW_State.Progress, 100);
                };

                progressDialog.Show();
                bwSave.RunWorkerAsync();
            }
        }

        #endregion

        #region Menu Items and Buttons

        private void miOpen_Click(object sender, EventArgs e) {
            OpenCSS();
        }

        private void miSave_Click(object sender, EventArgs e) {
            SaveCSS();
        }

        private void miClose_Click(object sender, EventArgs e) {
            if (!dirtyState || MessageBox.Show("Are you sure you want to abandon your progress on the current CSS file?", "CSS Image Embedder", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                lvFiles.Items.Clear();
                rtbCSS.Text = "";
                dirtyState = false;
                activeFileName = "";
                fileIsOpen = false;
                this.Text = originalTitle;
                UpdateUI();
                UpdateStats();
            }
        }

        private void miExit_Click(object sender, EventArgs e) {
            if (!dirtyState || MessageBox.Show("Are you sure you want to abandon your progress on the current CSS file?", "CSS Image Embedder", MessageBoxButtons.YesNo) == DialogResult.Yes)
                Application.Exit();
        }

        private void btnUnresolve_Click(object sender, EventArgs e) {
            ListViewItem item = lvFiles.SelectedItems[0];
            CSSReference url = (CSSReference)item.Tag;

            // Only reset the item if it is Resolved.
            if (url.Resolved) {
                item.SubItems[1].Text = "Unresolved";
                item.ForeColor = Color.Red;

                url.Resolved = false;
                url.RealPath = "";
                pbPreview.ImageLocation = "";
            }

            item.Tag = url;

            SetDirty();

            UpdateStats();
        }

        private void btnManuallyResolve_Click(object sender, EventArgs e) {
            ListViewItem item = lvFiles.SelectedItems[0];
            CSSReference url = (CSSReference)item.Tag;

            if (ofdOther.ShowDialog() == DialogResult.OK) {
                String path = ofdOther.FileName;

                item.SubItems[1].Text = "Resolved";
                item.ForeColor = Color.Green;
                url.Resolved = true;
                url.RealPath = path;
            }

            item.Tag = url;

            SetDirty();

            UpdateStats();
        }

        private void btnAutoResolve_Click(object sender, EventArgs e) {
            // Temporarily allow the user to select multiple images
            ofdOther.Multiselect = true;

            if (ofdOther.ShowDialog() == DialogResult.OK) {
                int items = lvFiles.Items.Count;

                // Iterate through each file the user selected
                foreach (String fullPath in ofdOther.FileNames) {
                    // Get the file name of this object
                    FileInfo info = new FileInfo(fullPath);

                    // Get the file name
                    String fileName = info.Name.ToLower().Trim();

                    // Look at each of the items to see if the name matches
                    // that of the current file
                    for (int i = 0; i < items; i++) {
                        ListViewItem item = lvFiles.Items[i];
                        CSSReference url = (CSSReference)item.Tag;

                        // Check names
                        if (url.ShortName == fileName) {
                            // Only resolve items that haven't already been resolved
                            if (!url.Resolved) {
                                url.RealPath = fullPath;
                                url.Resolved = true;

                                item.SubItems[1].Text = "Resolved";
                                item.ForeColor = Color.Green;

                                // Save changes
                                item.Tag = url;

                                SetDirty();
                            } else if (url.Resolved) {
                                // If this item has already been resolved, no need scanning
                                // the rest since duplicates are removed
                                break;
                            }
                        }
                    }
                }
            }

            ofdOther.Multiselect = false;

            UpdateStats();
        }

        private void miAutoResolve_Click(object sender, EventArgs e) {
            btnAutoResolve_Click(sender, e);
        }

        private void miAbout_Click(object sender, EventArgs e) {
            frmAbout about = new frmAbout();
            about.Show();
        }

        #endregion

        #region List View Events

        private void lvFiles_Leave(object sender, EventArgs e) {
            // Restore last selection to red, if it exists
            if (lastSelStart != -1) {
                rtbCSS.Select(lastSelStart, lastSelLen);
                rtbCSS.SelectionColor = Color.Red;

                lastSelLen = -1;
                lastSelStart = -1;
            }

            // Clear the preview picture box
            pbPreview.ImageLocation = "";

            UpdateUI();
        }

        private void lvFiles_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e) {
            // Highlights and scrolls to a selected file in the ListView

            UpdateUI();

            if (lvFiles.SelectedItems.Count != 1)
                return;

            ListViewItem item = lvFiles.SelectedItems[0];

            // Select in CSS view
            CSSReference url = (CSSReference)item.Tag;

            // If the selected file reference has been resolved, load it
            // in the picture box for preview.
            if (url.Resolved)
                pbPreview.ImageLocation = url.RealPath;

            // Restore last selection to red, if it exists
            if (lastSelStart != -1) {
                rtbCSS.Select(lastSelStart, lastSelLen);
                rtbCSS.SelectionColor = Color.Red;

                lastSelLen = -1;
                lastSelStart = -1;
            }

            // Remember this selection for next time (see above)
            lastSelStart = url.Start;
            lastSelLen = url.Length;

            // Highlight blue
            rtbCSS.Select(url.Start, url.Length);
            rtbCSS.SelectionColor = Color.Blue;
            rtbCSS.ScrollToCaret();
        }

        private void lvFiles_Enter(object sender, EventArgs e) {
            // When selecting an item that has just been deselected (after losing focus), the ItemSelectionChanged
            // event doesn't fire. So, upon entering the listview, invoke that event handler.
            // Just include a fake ListViewItemSelectionChangedEventArgs: none of it is used later.
            if (lvFiles.Items.Count > 0)
                lvFiles_ItemSelectionChanged(sender, new ListViewItemSelectionChangedEventArgs(lvFiles.Items[0], 0, false));
        }

        #endregion

        #region User Interface Management

        private void UpdateStats() {
            // Count how many items are resolved
            int resolved = 0;

            int total = lvFiles.Items.Count;

            for (int i = 0; i < total; i++) {
                CSSReference url = (CSSReference)lvFiles.Items[i].Tag;

                if (url.Resolved)
                    resolved++;
            }

            lblStats.Text = "Total: " + total + " | Resolved: " + resolved;
        }

        /// <summary>
        /// Manages the user interface options.
        /// </summary>
        private void UpdateUI() {
            // If there is no project currently open, disable Auto resolve button
            btnAutoResolve.Enabled = fileIsOpen;
            miAutoResolve.Enabled = btnAutoResolve.Enabled;
            miEmbedAndSave.Enabled = btnAutoResolve.Enabled;

            // If no items are selected, disable the buttons
            if (lvFiles.SelectedIndices.Count != 1) {
                btnUnresolve.Enabled = false;
                btnManuallyResovle.Enabled = false;
            } else {
                // If an item is selected, enable all the buttons
                btnUnresolve.Enabled = true;
                btnManuallyResovle.Enabled = true;
                UpdateStats();
            }
        }

        #endregion

        #region Utility

        /// <summary>
        /// Closes a progress dialog while keeping the current form on top.
        /// </summary>
        /// <param name="progress">The progress dialog to close.</param>
        private void CloseProgressDialog(frmProgress progress) {
            this.TopMost = true;
            progress.ForceClose();
            progress = null;
            this.TopMost = false;
        }

        #endregion

        #region DLL Imports

        // Gets the mime type of the file given a sample of the data
        [DllImport("urlmon.dll", CharSet = CharSet.Unicode, ExactSpelling = true, SetLastError = false)]
        public static extern int FindMimeFromData(IntPtr pBC, [MarshalAs(UnmanagedType.LPWStr)] string pwzUrl, [MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.I1, SizeParamIndex = 3)]byte[] pBuffer, int cbSize, [MarshalAs(UnmanagedType.LPWStr)]  string pwzMimeProposed, int dwMimeFlags, out IntPtr ppwzMimeOut, int dwReserved);

        #endregion

        #region Structs and Enums and Bears (Oh, my)

        public struct CSSReference {
            public int Start;
            public int Length;
            public String Name;
            public Boolean Resolved;
            public String ShortName;
            public String RealPath;
        }

        // For reporting progress type...
        // better than naked ints!
        enum BW_State {
            Progress = 1,
            Error = 2,
            Warning = 3,
            Done = 4,
            Canceled = 5,
            Notice = 6,
            Failure = 7,
            Result = 8
        }

        #endregion

    }
}
